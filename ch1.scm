(+ (* 3
      (+ (* 2 4)
         (+ 3 5)))
   (+ (- 10 7)
                 6))

(define (square x) (* x x))

(define bob 18)

(define (abs x)
  (cond ((> x 0) x)
        ((= x 0) 0)
        ((< x 0) (- x))))

(define (abs-2 x)
  (cond ((< x 0) (- x))
        (else x)))


(define (abs-3 x)
  (if (< x 0)
      (- x)
      x))

10

(+ 5 3 4)

(- 9 1)

(/ 6 2)

(+ (* 2 4) (- 4 6))

(define a 3)

(define b (+ a 1))

(+ a b (* a b))

(= a b)

(if (and (> b a) (< b (* a b)))
    b
    a)

(cond ((= a 4) 6)
      ((= b 4) (+ 6 7 a))
      (else 25))

(+ 2 (if (> b a) b a))

(* (cond ((> a b) a)
         ((< a b) b)
         (else -1))
                (+ a 1))


;; 5 + 4 + (2 - (3 - (6 + 4/5)))
;; -----------------------------
;;        3(6 - 2)(2 - 7)


(/ (+ 5
      4
      (- 2
         (- 3
            (+ 6
               (/ 4 5)))))
   (* 3
      (- 6 2 )
      (- 2 7)))

;; *Exercise 1.3:* Define a procedure that takes three numbers as
;; arguments and returns the sum of the squares of the two larger
;; numbers.

(define (largest-two x y z)
  (list (max x y)
        (max (min x y) z)))

(define (square x)
  (* x x))

(define (sum-of-squares x y)
  (+ (square x) (square y)))

(define (sum-of-squares-of-largest-two x y z)
  (apply sum-of-squares (largest-two x y z)))


;; *Exercise 1.4:* Observe that our model of evaluation allows for
;; combinations whose operators are compound expressions.  Use this
;; observation to describe the behavior of the following procedure:
;;
;;      (define (a-plus-abs-b a b)
;;        ((if (> b 0) + -) a b))


(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))


;; *Exercise 1.5:* Ben Bitdiddle has invented a test to determine
;; whether the interpreter he is faced with is using
;; applicative-order evaluation or normal-order evaluation.  He
;; defines the following two procedures:

;; (define (p) (p))

;; (define (test x y)
;;   (if (= x 0)
;;       0
;;       y))

;; Then he evaluates the expression

;; (test 0 (p))

;; What behavior will Ben observe with an interpreter that uses
;; applicative-order evaluation?  What behavior will he observe with
;; an interpreter that uses normal-order evaluation?  Explain your
;; answer.  (Assume that the evaluation rule for the special form
;;                  `if' is the same whether the interpreter is using normal or
;;                  applicative order: The predicate expression is evaluated first,
;;                  and the result determines whether to evaluate the consequent or
;;                       the alternative expression.)


(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))

;; (test 0 (p))

;; An applicative order interpreter will evaluate all operands,
;; causing the recursive call to p to recur forever. If using normal
;; order, it will defer evaluation of operands until they are needed.
;; In this case, it is never needed, so the test function would
;; evaluate to 0.


;; 1. Write a Scheme expression whose evaluation would result in an
;; error if `and` were a procedure, but actually will have a value
;; because `and` is a special form. Do the same for `or`.


(and '#t '#t '#t "ted")

;; Define a procedure `true-false` that takes one argument and returns
;; 1 if the argument is true, and 0 if the argument is false.

(define (true-false p)
  (if p
      1
      0))

(define (true-false-2 p)
  (cond (p 1)
        ((not p) 0)))

;; 3. Use the evaluation rule from section 1.1.3 to describe the
;; process of evaluating the expression `(* pi (* radius radius))`


;; 1) (* radius radius)
;; 2) (* pi radius^2)
;; 3) profit



(define (our-and a b)
  (if a
      b
      #f))

(define (our-or a b)
  (if a
      #t
      b))


;; (define (good-enough? guess x)
;;   (< (abs (- (square guess) x))
;;      0.001))

(define (good-enough? guess previous-guess)
  (< (abs (- guess previous-guess))
     0.001))

(define (average x y)
  (/ (+ x y)
     2))

(define (improve guess x)
  (average guess
           (/ x guess)))

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
        (else else-clause)))

(define (sqrt-iter guess previous-guess x)
  (new-if (good-enough? guess previous-guess)
      guess
      (sqrt-iter (improve guess x)
                 guess
                 x)))

(define (sqrt x)
  (sqrt-iter 1.0 0 x))

;; Write a procedure `power-close-to` that takes as arguments two
;; positive integers `b` and `n`, and returns the smallest power of
;; `b` that is greater than `n`. That is, it should return the
;; smallest integer `e` such that `b^e > n`. You may use the Scheme
;; procedure `expt`, which raises a given number to a given power:

(power-close-to 2 1000)
;; 10

(expt 2 10)
;; 1024


(define (improve guess)
  (+ guess 1))

(define (power-up* base guess target)
  (if (< target (expt base guess))
      guess
      (power-up* base
                 (improve guess)
                 target)))

(define (power-close-to base target)
  (power-up* base 0 target))

(define (A x y)
            (cond ((= y 0) 0)
                  ((= x 0) (* 2 y))
                  ((= y 1) 2)
                  (else (A (- x 1)
                           (A x (- y 1))))))

(define (fib n)
  (fib-iter 1 0 n))

(define (fib-iter a b count)
  (if (= count 0)
      b
      (fib-iter (+ a b) a (- count 1))))

(define (count-change amount)
  (cc amount 5))

(define (cc amount kinds-of-coins)
  (cond ((= amount 0) 1)
        ((or (< amount 0)
             (= kinds-of-coins 0))
         0)
        (else
         (+ (cc amount (- kinds-of-coins 1))
            (cc (- amount (first-denomination
                           kinds-of-coins))
                kinds-of-coins)))))

(define (first-denomination kinds-of-coins)
  (cond ((= kinds-of-coins 1) 1)
        ((= kinds-of-coins 2) 5)
        ((= kinds-of-coins 3) 10)
        ((= kinds-of-coins 4) 25)
        ((= kinds-of-coins 5) 50)))

;;     1
;;    1 1
;;   1 2 1
;;  1 3 3 1
;; 1 4 6 4 1

;; (pascal-row 0) => '(1)
;; (pascal-row 1) => '(1 1)
;; (pascal-row 2) => '(1 2 1)

;; (define (pascal row col)
;;   (cond ((> col row) (display "Whuddya want from me, bad value!"))
;;         ((= col 0) 1)
;;         ((= 1 col row) 1)
;;         ((= row col) 1)
;;         (else (+ (pascal (- row 1)
;;                          (- col 1))
;;                  (pascal (- row 1)
;;                          col)))))

;; (define (pascal-row n acc)
;;   (display (pascal n acc))
;;   (if (< acc n)
;;       (pascal-row n (+ 1 acc))
;;       (newline)))

;; (define (pascal-triangle* n acc)
;;   (pascal-row acc 0)
;;   (if (< acc n)
;;       (pascal-triangle* n (+ acc 1))))

(define (pascal n)
  (define (triangle n acc)
    (level acc 0)
    (if (< acc n)
        (triangle n (+ acc 1))))
  (define (level n acc)
    (display (element n acc))
    (if (< acc n)
        (level n (+ 1 acc))
        (newline)))
  (define (element row col)
    (cond ((> col row) (display "Whuddya want from me, bad value!"))
          ((= col 0) 1)
          ((= 1 col row) 1)
          ((= row col) 1)
          (else (+ (element (- row 1)
                     (- col 1))
                   (element (- row 1)
                     col)))))
  (triangle n 0))
